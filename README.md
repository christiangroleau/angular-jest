# Jest setup for Angular

*Note:* This guide covers Angular 12


## Replace Karma and Jasmine with Jest

Install the required Jest libraries

```
npm install --save-dev \
jest \
@types/jest \
jest-preset-angular
```

Uninstall Karma and Jasmine

```
npm uninstall \
karma \
karma-chrome-launcher \
karma-coverage \
karma-jasmine \
karma-jasmine-html-reporter \
@types/jasmine \
jasmine-core \
jasmine-spec-reporter
```


## Remove original test configurations

In `angular.json`, remove the `test` section:

```
"test": {
  "builder": "@angular-devkit/build-angular:karma",
  "options": {
    "main": "src/test.ts",
    "polyfills": "src/polyfills.ts",
    "tsConfig": "tsconfig.spec.json",
    "karmaConfig": "karma.conf.js",
    "assets": [
      "src/favicon.ico",
      "src/assets"
    ],
    "styles": [
      "src/styles.scss"
    ],
    "scripts": []
  }
}
```

Modify `tsconfig.spec.json` to something like the following:
```
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "outDir": "./out-tsc/spec",
    "types": [
      "jest",
      "node"
    ]
  },
  "files": [
    "src/polyfills.ts"
  ],
  "include": [
    "src/**/*.spec.ts",
    "src/**/*.d.ts"
  ]
}
```

Remove original test configuration files

```
rm karma.conf.js src/test.ts
```

## Add new Jest configurations

Create `setupJest.ts` with the following:
```
import 'jest-preset-angular/setup-jest';
```

In `tsconfig.json`, add the following configuration:
```
"allowSyntheticDefaultImports": true,
```

In `package.json`, modify the test section under scripts:
```
"test": "jest",
"test:coverage": "jest --coverage",
```

In `package.json`, add the following configuration to the end of the file:
```
"jest": {
  "preset": "jest-preset-angular",
  "setupFilesAfterEnv": [
    "<rootDir>/setupJest.ts"
  ],
  "testPathIgnorePatterns": [
    "<rootDir>/node_modules/",
    "<rootDir>/dist/"
  ],
  "globals": {
    "ts-jest": {
      "tsconfig": "<rootDir>/tsconfig.spec.json",
      "stringifyContentPathRegex": "\\.html$"
    }
  }
}
```

## Test the Jest configuration
`npm run test`


